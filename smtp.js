'use strict';

/* eslint-env es6 */
let nodemailer = require('nodemailer');

let async   = require('asyncawait/async');
let await   = require('asyncawait/await');
let mailin  = require('mailin');
let _       = require('lodash');
let app     = require('./app');
let User    = app.db.model('User');
let Project = app.db.model('Project');
let Email   = app.db.model('Email');
let config     = app.config.smtp;

let transporter = nodemailer.createTransport({
  host: 'smtp.nixsolutions.com',
  port: '25',
  secure: false,
  debug: true,
  ignoreTLS: true,
  auth: {
    user: '',
    pass: ''
  }
});


mailin.start({
  port: app.config.smtp.port || 2520,
  host: app.config.smtp.host,
  disableWebhook: true,
  disableDnsLookup: true,
  smtpOptions: {
    SMTPBanner: 'StubMail SMTP Server',
    secure: false,
    disabledCommands: ['AUTH']
  }
});

mailin.on('message', async.cps((connection, data, source) => {
  console.log('connection', connection);
  let email = _.pick(data, ['from', 'to', 'cc', 'subject', 'html', 'text', 'headers', 'messageId', 'date', 'attachments']);
  console.log('EMAIL=====================', email);
  email.source = source;

  console.log('email', email);

  let project = email._project = await(Project.findOne({title: email.headers['x-project']}).exec());
  let user    = email._user    = await(User.findOne({login: email.headers['x-user']}).exec());
  let external = email.headers['x-external'];

  if (~[user, project].indexOf(null)) {
    console.log('Empty');
    // @todo: need logging
    return;
  }

  // x-external
  if (external) {
    var message =  _.pick(data, ['from', 'to', 'cc', 'subject', 'html', 'text', 'messageId', 'date', 'attachments']);
    await(transporter.sendMail(message));
    console.log('MESSAGE==================', message);
  }
  await(Email.saveEmail.bind(Email, email));

}));
