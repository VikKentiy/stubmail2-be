'use strict';

let inquirer = require('inquirer');
let app      = require('../app');
let Project  = app.db.model('Project');

inquirer.prompt({
  type: 'input',
  name: 'project',
  message: 'Project name',
  validate: function(value) {
    console.log('Project', Project);

    if (!value) {
      return 'Project Title can\'t be empty';
    }
    return true;
  }
}, function(answer) {
  console.log(answer);
  let title = answer.project;
  console.log(title);
  let project = new Project({title: title});
  project.save(function(err, item) {
    if (err) {
      return console.log(err);
    }
    console.log(item);
  });
});
