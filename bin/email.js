'use strict';

var nodemailer = require('nodemailer');
var faker      = require('faker');
var app        = require('../app');
var config     = app.config.smtp;

 //Create a SMTP transporter object
var transporter = nodemailer.createTransport({
  host: config.host,
  port: config.port,
  secure: config.secure,
  debug: true,
  ignoreTLS: true
});

console.log('SMTP Configured');

// Message object
var message = {
  from: faker.name.findName() + '<' + faker.internet.email() + '>',
  to: faker.name.findName() + '<' + faker.internet.email() + '>',
  subject: faker.name.title(),
  headers: {
    'X-Project': '',
    'X-User': '',
    'X-External': true
},
  html: faker.lorem.paragraphs(),
  text: faker.lorem.paragraphs(),
  attachments: [
    {
      filename: 'image.png',
      content: new Buffer('iVBORw0KGgoAAAANSUhEUgAAABAAAAAQAQMAAAAlPW0iAAAABlBMVEUAAAD/' +
        '//+l2Z/dAAAAM0lEQVR4nGP4/5/h/1+G/58ZDrAz3D/McH8yw83NDDeNGe4U' +
        'g9C9zwz3gVLMDA/A6P9/AFGGFyjOXZtQAAAAAElFTkSuQmCC', 'base64'),
      cid: 'note@example.com' // should be as unique as possible
    },
    {
      filename: 'image.png',
      content: new Buffer('iVBORw0KGgoAAAANSUhEUgAAABAAAAAQAQMAAAAlPW0iAAAABlBMVEUAAAD/' +
        '//+l2Z/dAAAAM0lEQVR4nGP4/5/h/1+G/58ZDrAz3D/McH8yw83NDDeNGe4U' +
        'g9C9zwz3gVLMDA/A6P9/AFGGFyjOXZtQAAAAAElFTkSuQmCC', 'base64'),
      cid: 'note2@example.com' // should be as unique as possible
    }
  ]
};

console.log('Sending Mail');
transporter.sendMail(message, function(error, info) {
  if (error) {
    console.log('Error occurred');
    console.log(error);
    return;
  }
  console.log('Message sent successfully!');
  console.log('Server responded with "%s"', info.response);
});

