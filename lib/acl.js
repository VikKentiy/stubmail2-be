'use strict';

/**
 * @type {KoaRoles} Roles
 *
 * @param {Application} app
 * @returns {Generator}
 */
module.exports = app => {
  let Roles       = require('koa-roles');
  let _           = require('lodash');
  let permissions = require('../config/permissions.json5');

  _.each(permissions, (permission, role) => {
    if (!_.isArray(permission)) {
      permissions[role] = permission.permissions.concat(permissions[permission.extend]);
    }
  });

  /**
   * @type {KoaRoles} acl
   */
  let acl = new Roles({
    failureHandler: function*(action) {
      this.throw(403, `Access forbidden for: ${action}`);
    }
  });

  acl.use(function*(action) {
    // Admin
    var role = this.state.user && this.state.user.role;
    if (role === 'admin') {
      return true;
    }

    // Guest
    if (!this.state.user && ~permissions.guest.indexOf(action)) {
      return true;
    }

    // Others
    if (role && ~permissions[role].indexOf(action)) {
      return true;
    }
  });

  app.acl = acl;
  app.use(acl.middleware());
  return function*(next) {
    yield next;
  };
};
