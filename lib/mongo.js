'use strict';

/**
 * @param {Application} app
 * @returns {Generator}
 */
module.exports = app => {
  let mongoose = require('mongoose');
  let glob     = require('glob');

  app.db = mongoose;
  mongoose.connect(app.config.mongo.uri);
  glob.sync('../app/models/**.js', {cwd: __dirname}).forEach(require);

  return function*(next) {
    this.db = mongoose;
    yield next;
  };
};
