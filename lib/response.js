'use strict';

/**
 * @returns {Generator}
 */
module.exports = (app) => {
  let _ = require('lodash');

  return function*(next) {
    try {
      yield next;
    } catch (err) {
      console.log(err);
      this.status = err.status || 500;
      this.body = {
        name: err.name,
        message: err.message || 'Internal Error',
        stack: this.app.env === 'development' ? err.stack : ''
      };
      if (err.errors) {
        this.body.errors = _.map(err.errors, error => {
          return _.pick(error, 'message', 'path');
        })
      }
      if (this.errors) {
        this.body.errors = _.map(this.errors, error => {
          let path = _.keys(error)[0];
          return {
            path: path,
            message: error[path]
          };
        })
      }
      if (app.env === 'development') {
        console.log(JSON.stringify(this.body, null, 4));
      }
    }
  }
};
