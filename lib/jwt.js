'use strict';

/**
 * @param {Application} app
 * @returns {Generator}
 */
module.exports = app => {
  let jwt = require('koa-jwt');
  // @todo: remove unless
  app.use(jwt({secret: app.config.auth.jwt.secret, passthrough: true}).unless({path: ['/auth/login']}));

  return function*(next) {
    this.jwt = jwt;
    yield next;
  };
};
