'use strict';

/**
 * @returns {Generator}
 */
module.exports = () => {
  let Validator = require('koa-validate').Validator;
  let v         = require('validator');

  Validator.prototype.isObjectId = tip => {
    if (this.goOn && !v.isMongoId(this.value)) {
      this.addError(tip || 'id param is not id');
    }
    return this;
  };

  return function*(next) {
    yield next;
  }
};
