'use strict';

let Resource = require('koa-resource-router');
let _        = require('lodash');
let app      = require('../../app');
let Project  = app.db.model('Project');
let Email    = app.db.model('Email');
let User     = app.db.model('User');

module.exports = new Resource('projects', {
  index: [app.acl.can('projects:list'), function*() {
    let query = Project.find().populate('_users').sort('-created');
    if (this.state.user.role === User.ROLE_ADMIN) {
      return this.body = yield query.exec();
    }
    this.body = yield query.where('_users').equals(this.state.user._id).exec();
  }],

  create: [app.acl.can('projects:create'), function*() {
    this.checkBody('title').notEmpty().trim().toLow();
    if (this.errors) {
      this.throw(422, 'Invalid params');
    }
    let params = _.pick(this.request.body, Project.FIELDS);

    let revise = yield Project.findOne(params);
    if (revise) {
      this.throw(422, 'project with the same name already exists');
    } else {
      let project = new Project(params);
      yield project.save();
      this.body = project;
      this.status = 201;
    }
  }],

  show: [app.acl.can('projects:show'), projectParam, function*() {
    this.body = this.item;
  }],

  update: [app.acl.can('projects:update'), projectParam, function*() {
    this.checkBody('title').notEmpty().trim().toLow();
    if (this.errors) {
      this.throw(422, 'Invalid params');
    }

    let params = _.pick(this.request.body, Project.FIELDS);
    yield this.item.set(params).save();
    this.body = this.item;
  }],

  destroy: [app.acl.can('projects:destroy'), projectParam, function*() {
    yield this.item.remove();
    this.status = 204;
  }]
}).middleware();


function *projectParam(next) {
  this.checkParams('project').isObjectId();
  if (this.errors) {
    this.throw(422, 'Invalid Project id');
  }

  this.item = yield Project.findById(this.params.project);
  if (!this.item) {
    this.throw(404, 'Project not found');
  }
  yield next;
}
